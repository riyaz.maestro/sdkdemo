//
//  DashBoardPageViewController.swift
//  ViewAnimation
//
//  Created by Jaffer Sheriff U on 06/12/19.
//  Copyright © 2019 Riyaz Mohammed. All rights reserved.
//

import UIKit
import ZCUIFramework
import ZMLKit

class DashBoardPageViewController: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.title = "Dashboard"

        configurePage()
        setupSignoutButton()
      
        // Do any additional setup after loading the view.
    }
    private func setupSignoutButton()
    {
        let signOutButton = UIBarButtonItem.init(image: #imageLiteral(resourceName: "power"), style: UIBarButtonItem.Style.done, target: self, action: #selector(logoutClicked))
              self.navigationItem.rightBarButtonItem = signOutButton
        

    }
   private func configurePage(){
        let pageComponentDetails = ComponentDetail.init(componentLinkName: "New_DashBoard")
        let zmlPageVC = ZCUIService.getViewController(forPage: pageComponentDetails)
        zmlPageVC.view.translatesAutoresizingMaskIntoConstraints = false
        
        guard let superView = self.view else {
         return
        }
        self.addChild(zmlPageVC)
        superView.addSubview(zmlPageVC.view)
        
        NSLayoutConstraint.activate([
            zmlPageVC.view.leadingAnchor.constraint(equalTo: superView.leadingAnchor),
            zmlPageVC.view.trailingAnchor.constraint(equalTo: superView.trailingAnchor),
            zmlPageVC.view.topAnchor.constraint(equalTo: superView.topAnchor),
            zmlPageVC.view.bottomAnchor.constraint(equalTo: superView.bottomAnchor)
        ])
        
        
    }
    public func  reloadPage()
    {
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    

    
   @objc private  func logoutClicked()
    {
    let alert = UIAlertController.init(title: "Sign Out", message: "Are you sure, you want to sign out?", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { (action) in
            
        }))
        
        alert.addAction(UIAlertAction.init(title: "Sign Out", style: UIAlertAction.Style.destructive, handler: { (action) in
            
            if let mySceneDelegate = self.view.window?.windowScene?.delegate as? SceneDelegate
            {
                
                mySceneDelegate.logout()
            }
            
            //signout
        }))
        
        self.present(alert, animated: true, completion: nil)
    }

}
