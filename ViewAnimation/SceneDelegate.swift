//
//  SceneDelegate.swift
//  ViewAnimation
//
//  Created by Riyaz Mohammed on 26/11/19.
//  Copyright © 2019 Riyaz Mohammed. All rights reserved.
//

import UIKit
import ZCUIFramework
import ZMLKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        
        
        
        let scope = [ "ZohoCreator.meta.READ","ZohoCreator.data.READ","ZohoCreator.meta.CREATE",
                      "ZohoCreator.data.CREATE","aaaserver.profile.READ","ZohoContacts.userphoto.READ",
                      "ZohoContacts.contactapi.READ" ]
        let clientID = "10017299280.AT46A31FOVEJZREY0NDRDB2CNPQWPH"
        let clientSecret = "5bc8917d2bfcbefc37744fd38ac77944e3d51a76b7"
        let portalId = "10017299280"
        let urlScheme = "zylkercarchain://" //These parameters are retrieved from the URL mentioned in point 2
        let accountsUrl = "https://accounts.zohoportal.com" //enter the accounts URL of your respective DC. For eg: EU users use 'https://accounts.zohoportal.eu'.
        
        
        ZohoPortalAuth.initWithClientID(clientID, clientSecret: clientSecret, portalID: portalId,  scope: scope, urlScheme: urlScheme, mainWindow: self.window!, accountsPortalURL: accountsUrl)
        ZohoPortalAuth.getOauth2Token { (token, error) in
            if token == nil
            {
                 self.showLoginScreen()
                
            }else{
                // success login
                Creator.configure(delegate: self)
                ZCUIService.delegate = self
                self.showHome()
                
            }
        }

        guard let _ = (scene as? UIWindowScene) else { return }
    }
    func showHome()
    {
        ///homeVC
        let pageViewController = DashBoardPageViewController()
        let navigationController = UINavigationController.init(rootViewController: pageViewController)
        self.window?.rootViewController = navigationController
    }
    func showLoginScreen()
    {
        DispatchQueue.main.async {
        ZohoPortalAuth.presentZohoPortalSign(in: { (token, error) in
            if token != nil{
                // success login
                Creator.configure(delegate: self)
                //                            self.showBookingForm()
                ZCUIService.delegate = self
                self.showHome()
                
            }
            
        })

        }
    }
    func logout()
    {
        ZohoPortalAuth.revokeAccessToken { (error) in
            if error == nil
            {
                
                let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewController") as UIViewController
                
                self.window?.rootViewController = viewController
                self.showLoginScreen()
                
            }
        }
    }
    func showBookingForm(recordLinkId: String)
    {
        
        let formHandler = FormHandler(recordLinkId: recordLinkId)
        
        (self.window?.rootViewController as? UINavigationController)?.pushViewController(formHandler.bookingForm(), animated: true)
        
    }
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }
    
    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }
    
    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }
    
    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }
    
    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }
    
    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>)
    {
        if let context = URLContexts.first
        {
            let _ = ZohoPortalAuth.handleURL(context.url,
                                             sourceApplication: context.options.sourceApplication,
                                             annotation: context.options.annotation)
        }
    }
    
}

extension SceneDelegate: ZCCoreFrameworkDelegate
{
    func oAuthToken(with completion: @escaping AccessTokenCompletion) {
        ZohoPortalAuth.getOauth2Token { (token, error) in
            completion(token,error)
        }
    }
}

extension SceneDelegate: ZCUIServiceDelegate {
    
    func openURL(for openURLTasks: [OpenUrlTask]) {
        
        if  let openURLTask = openURLTasks.first,
            case OpenUrlType.componentUrl(let componenet) = openURLTask.urlType,
            let queryString = componenet.queryString?.components(separatedBy: "&") {
            
            for param in queryString {
                
                let key_value = param.components(separatedBy: "=")
                if  componenet.predictedComponentType == .form,
                    key_value.first == "For_Vehicle",
                    let value = key_value.last {
                    
                    showBookingForm(recordLinkId: value)
                }
                if  componenet.predictedComponentType == .page,
                    key_value.first == "New_Dashboard"{
                    
                    showHome()
                }
            }
        }
    }
    
}
