//
//  FormHandler.swift
//  ViewAnimation
//
//  Created by Thanga Ayyanar A on 06/12/19.
//  Copyright © 2019 Riyaz Mohammed. All rights reserved.
//

import Foundation
import ZCCoreFramework

class FormHandler : BookServiceDelegate,BookServiceDataSource
{
    
    var form: Form?
    var data: Data?
    let recordLinkId: String
    var vechicleData: VechicleData?
    var bookViewController: BookServiceViewController?
    
    init(recordLinkId: String) {
        
        self.recordLinkId = recordLinkId
        fetch_report()
    }
    
    func bookingForm() -> UIViewController {
        
        let book = BookServiceViewController()
        book.delegate = self
        book.dataSource = self
        
        self.bookViewController = book
        
        return book
    }
    
    func formFetch() {
        
        let formInfo = FormInfo(linkName: "Book_now", displayName: "Book now")
               
        
                //Fetch form API
               ZCFormAPIService.fetch(for: formInfo) { (result) in
                   
                   switch result {
                       
                   case .sucess(let workFlowChangeSet):
                       
                       // Get the Form model
                    self.form = self.vechicleSelected(form: workFlowChangeSet.alteredForm)
                       
                    self.bookViewController?.formFetched()
                       
                   case .failure(let error):
                       
                       print("Error occured: \(error)")
                   }
                   
               }
    }
    
    
    func vechicleSelected(form: Form) -> Form {
        
        var form = form
        let linkName = "For_Vehicle"
        
        if let result = form.getFieldAndIndex(using: FieldAccessPath.field(linkName: linkName)){
            
            let (field,index) = result
            
            guard var choiceField = field as? SingleSelectChoiceFieldProtocol else {
                    return form
            }
            
            choiceField.fieldValue.selectedChoice = choiceField.fieldValue.choices.filter{ $0.key == recordLinkId }.first
            
            form.setField(choiceField, at: index)
            
        }
        return form
    }
    
    func choiceSelected(atIndex: Int, fieldType: BookServiceField.FieldType) {
        
        let linkName = fieldType.linkName()
        
        if let result = form?.getFieldAndIndex(using: FieldAccessPath.field(linkName: linkName)){
            
            let (field,index) = result
            
            guard var choiceField = field as? SingleSelectChoiceFieldProtocol,
                atIndex < choiceField.fieldValue.choices.count  else {
                    return
            }
            
            choiceField.fieldValue.selectedChoice = choiceField.fieldValue.choices[atIndex]
            
            form?.setField(choiceField, at: index)
            
        }
        
    }
    
    func dateSelected(date: Date, fieldType: BookServiceField.FieldType) {
        
        let linkName = fieldType.linkName()
        
        if let result = form?.getFieldAndIndex(using: FieldAccessPath.field(linkName: linkName)){
            
            let (field,index) = result
            
            guard var dateField = field as? DateTimeField  else {
                    return
            }
            
            dateField.fieldValue.value = date.toString(dateFormat: "dd-MMM-yyyy hh:mm:ss")
            
            form?.setField(dateField, at: index)
            
        }
    }
    
    func choices(fieldType: BookServiceField.FieldType) -> [String] {
        
        switch fieldType {
        
        case .ServiceCentre,
             .ServiceType:
            
            let linkName = fieldType.linkName()
            if let fieldValue = getChoices(linkName: linkName){
                
                return fieldValue.choices.compactMap{ $0.value }
            }
            
        default:
            break
        }
        
        return []
    }
    
    func submitForm() {
        
        guard let form = form else {
            return
        }
        
        ZCFormAPIService.submit(form: form) { (result) in
            
            switch result{
                
            case .sucess(let successResponse):
                
                self.bookViewController?.formSubmitted()
                dump(successResponse)
                
            case .failure(let errorResponse):
                
                dump(errorResponse)
                
            }
            
        }
    }
    
    func selectedChoiceIndex(fieldType: BookServiceField.FieldType) -> Int? {
        
        switch fieldType {
        
        case .ServiceCentre,
             .ServiceType:
            
            let linkName = fieldType.linkName()
            if  let field = form?.getField(linkName: linkName) as? SingleSelectChoiceFieldProtocol,
                let value = field.fieldValue.selectedChoice{
                
                return field.fieldValue.choices.enumerated().filter{ value.key == $0.element.key }.map{ $0.offset }.first
            }
            
        default:
            break
        }
        return nil
    }
    
    func selectedDate(fieldType: BookServiceField.FieldType) -> Date? {
        
        let linkName = fieldType.linkName()
        if  let field = form?.getField(linkName: linkName),
            case InputValues.textFieldValue(let fieldValue) = field.getValue() {
            
            return Date.date(string: fieldValue, format: "dd-MMM-yyyy hh:mm:ss")
        }
        return nil
    }
    
    func dataForVechicleCell() -> VechicleData? {
        
        if  let data = data,
            var vechicleData = vechicleData {
            
            vechicleData.image = data
            return vechicleData
        }
        return nil
    }
    
    
}

// Vehicle record related operations

extension FormHandler {
    
    func fetch_report(){
        
//        let recordId = "3909551000000029011"
        
        let preConfiguration = ReportAPIConfiguration(needViewMeta: false, recordID: recordLinkId, reportHierarchy: nil, deviceType: .iPhone, columns: [], queryItems: nil, queryString: nil, setCriteria: false, isOffline: false)
        
        let configuration = ListReportAPIConfiguration(fromIndex: 1, limit: 1, moreInfo: preConfiguration)
        let reportInfo =  ReportInfo(openUrlInfo: nil, linkName: "All_Vehicle_Infos", appDisplayname: "Vechicle", displayName: "All Vechicle", notificationEnabled: false)
        
        
        ZCReportAPIService.fetchListReport(for: reportInfo, with: configuration, completionHandler: { (result) in
            
            switch result {
                
            case .sucess(let successResponse):
                
                do {
                    
                    if let column = try successResponse.records.first?.recordValue(for: "Vehicle_Image") {
                        
                        self.getImage(imageRecordValue: column)
                    }
                    var vechicleName = ""
                    var registerationNumber = ""
                    if let column = try successResponse.records.first?.recordValue(for: "Vehicle_Model") {
                        
                        if case let RecordValue.Value.text(text) = column.value {
                            
                            vechicleName = text.value
                        }
                    }
                    if let column = try successResponse.records.first?.recordValue(for: "Registration_Number") {
                        
                        if case let RecordValue.Value.text(text) = column.value {
                            
                            registerationNumber = text.value
                        }
                    }
                    self.vechicleData = VechicleData(name: vechicleName, regNo: registerationNumber, image: Data())
                }
                catch(let error) {
                    
                    print("Error occured in parsing report \(error)")
                }
                
            case .failure(_):
                
                print("Report: Error Occured")
            }
        })
    }
    
    
    func getImage(imageRecordValue: RecordValue) {
        
        ZCReportAPIService.downloadMedia(from: imageRecordValue){ (result) in
            
            switch result {
                
            case .sucess(let data):
                
                self.data = data
                self.formFetch()
                
            case .failure(let error):
                
                print("Media: Error occcured \(error)")
            }
        }
    }
}

//Helpers

extension FormHandler {
    
    func getChoices(linkName: String) -> SingleSelectChoiceFieldValue? {
        
        let fieldAccessPath = FieldAccessPath.field(linkName: linkName)
        if  let value = form?.getField(using: fieldAccessPath)?.getValue(),
            case let InputValues.singleSelectChoiceFieldValue(fieldValue) = value{
            
            return fieldValue
        }
        return nil
    }
}

