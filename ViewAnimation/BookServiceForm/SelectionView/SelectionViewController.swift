//
//  SelectionViewController.swift
//  ViewAnimation
//
//  Created by Riyaz Mohammed on 26/11/19.
//  Copyright © 2019 Riyaz Mohammed. All rights reserved.
//

import UIKit

protocol SelectionViewControllerDelegate {
    
    func heightOFTopView() -> Int
    func currentInputFieldType() -> BookServiceField.FieldType
    
    func choices(fieldType : BookServiceField.FieldType) -> [String]?
    func selectedChoice(fieldType : BookServiceField.FieldType, index: Int)
    func selectedDate(fieldType :BookServiceField.FieldType , date : Date)
    func initialChoiceIndex(fieldType :BookServiceField.FieldType ) -> Int?
    func initialDate(fieldType :BookServiceField.FieldType)-> Date?
    
    
}

class SelectionViewController: UIViewController {
    
    
    private enum AnimationOption
    {
        enum  Shift {
            case Up
            case Down
            func tyPoint() ->CGFloat
            {
                switch self {
                case .Up:
                    return -200
                case .Down:
                    return 70
                }
            }
        }
        
        enum  Render {
            case FromBottom
            case FromTop
            
            func tyPoint() ->CGFloat
            {
                switch self {
                case .FromBottom:
                    return 50
                case .FromTop:
                    return -50
                }
            }
            
        }
    }
    
    private let tableView = UITableView()
    private let datePicker = UIDatePicker()
    private let titleLabel = UILabel()
    private let doneButton = UIButton()
    
    private var inputFieldType : BookServiceField.FieldType?
    
    
    private var titleTopAnchorConstraint : NSLayoutConstraint?
    var delegate : SelectionViewControllerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white
        setupTitleLabel()
        setupDoneButton()
        setupTableView()
        setupDatePicker()
        
        showForFieldType(type: .ServiceCentre)

    }
    private func setupTitleLabel()
    {
        titleLabel.text = "Select Center"
        titleLabel.textColor = UIColor(red: 0.298, green: 0.29, blue: 0.471, alpha: 1)
        titleLabel.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.semibold)
        self.view.addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        titleTopAnchorConstraint = titleLabel.topAnchor.constraint(equalTo: self.view.topAnchor, constant: topMarginForTitle())
        
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 50),
            titleLabel.trailingAnchor.constraint(lessThanOrEqualTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -50),
            titleTopAnchorConstraint!,
            titleLabel.heightAnchor.constraint(equalToConstant: 19),
            //            titleLabel.bottomAnchor.constraint(lessThanOrEqualTo: self.view.bottomAnchor)
        ])
        
    }
    private func setupDoneButton()
    {
        
        doneButton.setTitle("Done", for: UIControl.State.normal)
        doneButton.addTarget(self, action: #selector(doneButtonClicked), for: UIControl.Event.touchUpInside)
        doneButton.setTitleColor(UIColor(red: 0.298, green: 0.29, blue: 0.471, alpha: 1), for: UIControl.State.normal)
        doneButton.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.semibold)
        self.view.addSubview(doneButton)
        doneButton.translatesAutoresizingMaskIntoConstraints = false
        
        
        NSLayoutConstraint.activate([
            doneButton.leadingAnchor.constraint(greaterThanOrEqualTo: titleLabel.trailingAnchor, constant: 30),
            doneButton.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -50),
            doneButton.topAnchor.constraint(equalTo: titleLabel.topAnchor),
            doneButton.heightAnchor.constraint(equalToConstant: 19),
            //            doneButton.bottomAnchor.constraint(equalTo: titleLabel.bottomAnchor)
        ])
        
    }
    @objc private func doneButtonClicked()
    {
        
         guard let fieldType = inputFieldType else
                {
                    return
                }
        animateOldInputView(option: SelectionViewController.AnimationOption.Shift.Up)
                guard let newField = BookServiceField.FieldType.init(rawValue: fieldType.rawValue + 1) else { return
                }
                showForFieldType(type: newField)
        animateNewInputView(option: SelectionViewController.AnimationOption.Render.FromBottom)
                delegate?.selectedDate(fieldType:BookServiceField.FieldType.PickupTime, date: datePicker.date)
    }
    private func setupTableView()
    {
        tableView.register(UINib(nibName: "SelectionTableViewCell", bundle: nil), forCellReuseIdentifier: "SelectionTableViewCell")
        
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        self.view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor, constant: 0),
            tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -50),
            tableView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 5),
            tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
        ])
        tableView.isHidden = true
        
    }
    private func setupDatePicker()
    {
        datePicker.datePickerMode = .dateAndTime
        datePicker.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(datePicker)
        NSLayoutConstraint.activate([
            datePicker.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor, constant: 0),
            datePicker.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -50),
            datePicker.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20),
            datePicker.bottomAnchor.constraint(lessThanOrEqualTo: self.view.bottomAnchor),
            datePicker.heightAnchor.constraint(equalToConstant: 200)
        ])
        datePicker.isHidden = true
    }
    private func topMarginForTitle() -> CGFloat
    {
        let padding  = 20
        let heightOFTopView = delegate?.heightOFTopView() ?? 80
        
        let margin = padding + heightOFTopView
        
        return CGFloat(margin)
    }
    
    private func showForFieldType(type : BookServiceField.FieldType)
    {
        inputFieldType = type
        titleLabel.text = titleText()
        
        if type == .PickupTime
        {
            titleLabel.isHidden = false
            
            tableView.isHidden = true
            datePicker.isHidden = false
            doneButton .isHidden = false
            
          //  UIView.animate(withDuration: 0.3) {
            self.view.backgroundColor = UIColor.white
         //   }
            datePicker.date = delegate?.initialDate(fieldType: BookServiceField.FieldType.PickupTime) ?? Date()
            
        }
        else if type == .none
        {
            tableView.isHidden = true
            titleLabel.isHidden = true
            doneButton.isHidden = true
            datePicker.isHidden = true
            UIView.animate(withDuration: 0.3) {
            self.view.backgroundColor = UIColor.clear
            }

            
        }
        else
        {
            UIView.animate(withDuration: 0.3) {
                self.tableView.alpha = 1
                self.titleLabel.isHidden = false
                self.tableView.isHidden = false
                self.datePicker.isHidden = true
                self.doneButton .isHidden = true
                self.view.backgroundColor = UIColor.white
            }
            
        }
        
        
    }
    
    
   
 public func refreshPositionOnFieldValueCorrection()
    {
        animateOldInputView(option: SelectionViewController.AnimationOption.Shift.Down)
        animateNewInputView(option: SelectionViewController.AnimationOption.Render.FromTop)
        
        if  delegate != nil
        {
            showForFieldType(type: delegate!.currentInputFieldType())
        }

        tableView.reloadData()
    }
    
      
    public func refreshPositionOnFieldValueCorrectionAfterAllInputisFilled()
       {
           animateNewInputView(option: SelectionViewController.AnimationOption.Render.FromTop)
           
           if  delegate != nil
           {
               showForFieldType(type: delegate!.currentInputFieldType())
           }
        
        tableView.reloadData()

       }

    
    private func choices() -> [String]
    {
        
        guard let fieldType = inputFieldType else
        {
            return [String]()
        }
        
        return delegate?.choices(fieldType:fieldType) ?? [String]()
        
    }
    private func titleText() ->String
    {
        
        guard let fieldType = inputFieldType else
        {
            return ""
        }
        
        return fieldType.title()
        
    }
    private func cellTitleText(index : Int) -> (String,String)?
    {
        let choiceString =  choices()[index]
        let strings = choiceString.components(separatedBy: " - ")
        if strings.count > 1
        {
            return (strings[0],strings[1])
        }
        return nil
    }
    
    
    
}

extension SelectionViewController : UITableViewDataSource,UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return choices().count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let  cell  = tableView.dequeueReusableCell(withIdentifier: "SelectionTableViewCell", for: indexPath) as! SelectionTableViewCell
        
        guard let titles = cellTitleText(index: indexPath.row) else {
            cell.customTitleLabel.text = ""
            cell.customSubtitleLabel.text = ""
            return cell
        }
        
        
        cell.customTitleLabel.text = titles.0
        cell.customSubtitleLabel.text = titles.1
        
        
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)

        guard let fieldType = inputFieldType else
        {
            return
        }
        
        animateOldInputView(option: SelectionViewController.AnimationOption.Shift.Up)
//        shiftSnapShotUP()
        
        guard let newField = BookServiceField.FieldType.init(rawValue: fieldType.rawValue + 1) else { return
        }
        showForFieldType(type: newField)
        animateNewInputView(option: SelectionViewController.AnimationOption.Render.FromBottom)
//        presentContentsUP()
        self.delegate?.selectedChoice(fieldType: fieldType, index: indexPath.row)
        tableView.reloadData()
        
        
    }
}

extension SelectionViewController
{

    private func animateOldInputView(option : AnimationOption.Shift){

         UIGraphicsBeginImageContext(self.view.bounds.size)
            let context = UIGraphicsGetCurrentContext()!
            self.view.layer.render(in: context)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            let viewSnapShot = UIImageView(image: image)
            self.view.layer.addSublayer(viewSnapShot.layer)

            viewSnapShot.alpha = 1
            UIView.animate(withDuration: 0.3, animations: {
                viewSnapShot.alpha = 0
                let transform = CATransform3DTranslate(CATransform3DIdentity, 0, option.tyPoint(), 0)
                viewSnapShot.layer.transform = transform
                
            }) { (suc) in
                viewSnapShot.removeFromSuperview()
            }
        
    
        
        }

    
  private  func animateNewInputView(option : AnimationOption.Render)
    {  self.tableView.alpha = 0
                          self.datePicker.alpha = 0
                          self.titleLabel.alpha = 0
                          self.doneButton.alpha = 0
                UIView.animate(withDuration: 0, delay:0.1, animations: {
        //            self.titleTopAnchorConstraint?.constant = self.topMarginForTitle() + 200
                    self.tableView.alpha = 0
                    self.datePicker.alpha = 0
                    self.titleLabel.alpha = 0
                    self.doneButton.alpha = 0
                    let transform = CATransform3DTranslate(CATransform3DIdentity, 0, option.tyPoint(), 0)
                    self.tableView.layer.transform = transform
                    self.titleLabel.layer.transform = transform
                    self.doneButton.layer.transform = transform
                    self.datePicker.layer.transform = transform
                }) { (suc) in

                    UIView.animate(withDuration: 0.3, delay:0.0, animations: {
                        self.tableView.alpha = 1
                        self.titleLabel.alpha = 1
                        self.doneButton.alpha = 1
                        self.datePicker.alpha = 1
                        self.titleTopAnchorConstraint?.constant = self.topMarginForTitle()

                        let transform = CATransform3DTranslate(CATransform3DIdentity, 0, 0, 0)
                        self.tableView.layer.transform = transform
                        self.titleLabel.layer.transform = transform
                        self.doneButton.layer.transform = transform
                        self.datePicker.layer.transform = transform

                    }) { (suc) in
                    }

                }
    }
    /*
    private func shiftSnapShotUP()
       {
           UIGraphicsBeginImageContext(self.view.bounds.size)
           let context = UIGraphicsGetCurrentContext()!
           self.view.layer.render(in: context)
           let image = UIGraphicsGetImageFromCurrentImageContext()
           UIGraphicsEndImageContext()
           let viewSnapShot = UIImageView(image: image)
           self.view.layer.addSublayer(viewSnapShot.layer)

           viewSnapShot.alpha = 0.9
           UIView.animate(withDuration: 0.3, animations: {
               viewSnapShot.alpha = 0
               let transform = CATransform3DTranslate(CATransform3DIdentity, 0, -200, 0)
               viewSnapShot.layer.transform = transform
               
           }) { (suc) in
               viewSnapShot.removeFromSuperview()
           }
           
           
       }
       
       func presentContentsUP() {
                   UIView.animate(withDuration: 0, delay:0.1, animations: {
           //            self.titleTopAnchorConstraint?.constant = self.topMarginForTitle() + 200
                       self.tableView.alpha = 0
                       self.datePicker.alpha = 0
                       self.titleLabel.alpha = 0
                       self.doneButton.alpha = 0
                       let transform = CATransform3DTranslate(CATransform3DIdentity, 0, 50, 0)
                       self.tableView.layer.transform = transform
                       self.titleLabel.layer.transform = transform
                       self.doneButton.layer.transform = transform
                       self.datePicker.layer.transform = transform
                   }) { (suc) in

                       
                       UIView.animate(withDuration: 0.3, delay:0.0, animations: {
                           self.tableView.alpha = 1
                           self.titleLabel.alpha = 1
                           self.doneButton.alpha = 1
                           self.datePicker.alpha = 1
                           self.titleTopAnchorConstraint?.constant = self.topMarginForTitle()

                           let transform = CATransform3DTranslate(CATransform3DIdentity, 0, 0, 0)
                           self.tableView.layer.transform = transform
                           self.titleLabel.layer.transform = transform
                           self.doneButton.layer.transform = transform
                           self.datePicker.layer.transform = transform

                       }) { (suc) in
                       }

                   }
       }
       
        private func shiftTableDown2(completion : @escaping (Bool) -> Void )
        {
            
            UIGraphicsBeginImageContext(self.view.bounds.size)
            let context = UIGraphicsGetCurrentContext()!
            self.view.layer.render(in: context)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            let viewSnapShot = UIImageView(image: image)
            self.view.layer.addSublayer(viewSnapShot.layer)
            
            
            
            viewSnapShot.alpha = 1
            UIView.animate(withDuration: 0.3, animations: {
                viewSnapShot.alpha = 0.0
                let transform = CATransform3DTranslate(CATransform3DIdentity, 0, 70, 0)
                viewSnapShot.layer.transform = transform
                
            }) { (suc) in
                viewSnapShot.removeFromSuperview()
            }
            

            
            
            UIView.animate(withDuration: 0, delay:0.1, animations: {
    //            self.titleTopAnchorConstraint?.constant = self.topMarginForTitle()
                self.tableView.alpha = 0
                self.datePicker.alpha = 0
                self.titleLabel.alpha = 0
                self.doneButton.alpha = 0
                let transform = CATransform3DTranslate(CATransform3DIdentity, 0, -50, 0)
                self.tableView.layer.transform = transform
                self.titleLabel.layer.transform = transform
                self.doneButton.layer.transform = transform
                self.datePicker.layer.transform = transform

            }) { (suc) in

                
                UIView.animate(withDuration: 0.3, delay:0.0, animations: {
                    self.tableView.alpha = 1
                    self.titleLabel.alpha = 1
                    self.doneButton.alpha = 1
                    self.datePicker.alpha = 1
                    self.titleTopAnchorConstraint?.constant = self.topMarginForTitle()

                    let transform = CATransform3DTranslate(CATransform3DIdentity, 0, 0, 0)
                    self.tableView.layer.transform = transform
                    self.titleLabel.layer.transform = transform
                    self.doneButton.layer.transform = transform
                    self.datePicker.layer.transform = transform

                }) { (suc) in
                }

            }

            
            
        }

*/
    

}
