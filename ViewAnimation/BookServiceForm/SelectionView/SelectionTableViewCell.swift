//
//  SelectionTableViewCell.swift
//  ViewAnimation
//
//  Created by Riyaz Mohammed on 28/11/19.
//  Copyright © 2019 Riyaz Mohammed. All rights reserved.
//

import UIKit

class SelectionTableViewCell: UITableViewCell {

    @IBOutlet weak var customTitleLabel: UILabel!
    @IBOutlet weak var customSubtitleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
