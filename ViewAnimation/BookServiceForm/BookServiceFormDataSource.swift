//
//  BookServiceDataSource.swift
//  ViewAnimation
//
//  Created by Riyaz Mohammed on 28/11/19.
//  Copyright © 2019 Riyaz Mohammed. All rights reserved.
//

import UIKit

protocol BookServiceDataSource {

    func choices(fieldType : BookServiceField.FieldType) -> [String]
    func submitForm()
    func selectedChoiceIndex(fieldType : BookServiceField.FieldType) -> Int?
    func selectedDate(fieldType : BookServiceField.FieldType) -> Date?
    func dataForVechicleCell() -> VechicleData?
}


protocol BookServiceDelegate {

    func choiceSelected(atIndex : Int , fieldType : BookServiceField.FieldType)
    func dateSelected(date : Date , fieldType : BookServiceField.FieldType)

}
