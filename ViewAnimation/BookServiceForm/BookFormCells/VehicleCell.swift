//
//  VehicleCell.swift
//  ViewAnimation
//
//  Created by Riyaz Mohammed on 26/11/19.
//  Copyright © 2019 Riyaz Mohammed. All rights reserved.
//

import UIKit


struct VechicleData {
    
    let name: String
    let regNo: String
    var image: Data
}


class VehicleCell:  FieldBaseCell {
    @IBOutlet weak var insideContentView: UIView!
    
    @IBOutlet weak var vehicleImageView: UIImageView!
    @IBOutlet weak var secondaryTitleLabel: UILabel!
    @IBOutlet weak var primaryTitleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.configure()
//        self.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner ,.layerMinXMinYCorner,.layerMaxXMinYCorner]
        
        self.insideContentView.layer.cornerRadius = 20
//        insideContentView.layer.borderWidth = 2
        self.insideContentView.clipsToBounds = true
    }
    
    func setVehicleInfo(data : VechicleData?)
    {
        guard let veh = data else
        {
            vehicleImageView.image = nil
            primaryTitleLabel.text = ""
            secondaryTitleLabel.text = ""
            return
        }
        
        vehicleImageView.image = UIImage.init(data: veh.image)
        primaryTitleLabel.text = veh.name
        secondaryTitleLabel.text = veh.regNo
        
    }

    override func topAnchorConstraintConstant() -> Float {
          return 0
      }
    override func configureShadowView() {
        
        super.configureShadowView()
        shaddowView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner ,.layerMinXMinYCorner,.layerMaxXMinYCorner]


    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
