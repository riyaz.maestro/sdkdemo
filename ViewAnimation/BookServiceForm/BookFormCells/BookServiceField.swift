//
//  BookServiceField1.swift
//  ViewAnimation
//
//  Created by Riyaz Mohammed on 28/11/19.
//  Copyright © 2019 Riyaz Mohammed. All rights reserved.
//

import  UIKit

struct BookServiceField
{
    enum FieldType : Int {
        case Vehicle = 0
        case ServiceCentre = 1
        case ServiceType = 2
        case PickupTime = 3
        case `none` = 4
        
        
        func title () ->String
        {
            switch self {
            case .Vehicle:
                return ""
            case .ServiceCentre:
                return "Service Center"
            case .ServiceType:
                return "Service type"
            case .PickupTime:
                return "Pickup Date & Time"
            case .none:
                return ""
            }
        }
        
        func linkName() -> String {
            
            switch self {
            case .Vehicle:
                return ""
            case .ServiceCentre:
                return "Choose_Service_Centre"
            case .ServiceType:
                return "Service_Type"
            case .PickupTime:
                return "Pickup_Time"
            case .none:
                return ""
            }
        }
        
        
    }
    
    
    
}

enum BookServiceCellColor : Int {
    
    case Vehicle = 0
    case SeriveCentre = 1
    case ServiceType = 2
    case PickupDate = 3
    case BookButton = 4
    
    
    
    func color() -> UIColor
    {
        switch  self {
        case .Vehicle:
            return  UIColor.white
        case .SeriveCentre:
            return  UIColor(red: 255.0/255.0, green: 254.0/255.0, blue: 252.0/255.0, alpha: 1)
        case .ServiceType:
            return UIColor(red: 255.0/255.0, green: 252.0/255.0, blue: 250.0/255.0, alpha: 1)
        case .PickupDate:
            return  UIColor(red: 255.0/255.0, green: 251.0/255.0, blue: 247.0/255.0, alpha: 1)
        case .BookButton:
            return UIColor(red: 255.0/255.0, green: 151.0/255.0, blue: 75.0/255.0, alpha: 1)
            
        }
        
    }
    
}



protocol BookServiceFieldCell : UITableViewCell
{
    func setIndex(index :Int)
    func topAnchorConstraintConstant() -> Float
}
extension BookServiceFieldCell where Self : UITableViewCell
{
    
}

class FieldBaseCell: UITableViewCell , BookServiceFieldCell{
    
    
    var shaddowView = UIView()
    
    
    func configure()
    {
        contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        configureShadowView()
        
    }
    func setIndex(index: Int) {
        shaddowView.backgroundColor =  BookServiceCellColor(rawValue: (index))?.color()
        
    }
    func topAnchorConstraintConstant() -> Float {
        return -25
    }
    
    
    func configureShadowView()
    {
        
        shaddowView.backgroundColor = UIColor.white
        shaddowView.layer.masksToBounds = false
        shaddowView.layer.shadowOpacity = 0.1
        shaddowView.layer.shadowRadius = 3
        shaddowView.layer.shadowOffset = CGSize(width: 0, height: 2)
        shaddowView.layer.shadowColor = UIColor.black.cgColor
        shaddowView.layer.cornerRadius = 20
        shaddowView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        
        shaddowView.translatesAutoresizingMaskIntoConstraints = false
        shaddowView.layer.cornerRadius = 20
        shaddowView.clipsToBounds = false
        
        shaddowView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        self.addSubview(shaddowView)
        
        self.bringSubviewToFront(contentView)
        
        NSLayoutConstraint.activate([
            
            shaddowView.topAnchor.constraint(equalTo: self.topAnchor,constant:CGFloat(topAnchorConstraintConstant())),
            shaddowView.bottomAnchor.constraint(equalTo: self.bottomAnchor,constant: -5),
            
            shaddowView.leadingAnchor.constraint(equalTo: self.leadingAnchor  , constant: 5),
            shaddowView.trailingAnchor.constraint(equalTo: self.trailingAnchor,constant:  -5),
        ])
        
    }
    
}




class ChoiceFieldCell: FieldBaseCell {
    
    let customTextLabel = UILabel()
    let customDetailedTextLabel = UILabel()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.configure()
    }
    
    override func configure()
    {
        super.configure()

        customTextLabel.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.medium)
        customDetailedTextLabel.font =  UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.medium)
        customTextLabel.textColor  = UIColor(red: 0.161, green: 0.286, blue: 0.341, alpha: 0.7)
        customDetailedTextLabel.textColor = UIColor(red: 0.161, green: 0.286, blue: 0.341, alpha: 1)
        
        
        let stack  = UIStackView.init()
        stack.alignment =  .leading
        stack.axis = .vertical
        stack.spacing = 6
        stack.translatesAutoresizingMaskIntoConstraints = false
        
        stack.addArrangedSubview(customTextLabel)
        stack.addArrangedSubview(customDetailedTextLabel)
        
        
        self.contentView.addSubview(stack)
        
        NSLayoutConstraint.activate([
            stack.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor, constant:  -5),
//            stack.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor),
            
            stack.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 25),
            stack.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -25),
//            stack.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 10),
//            stack.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -20),
        ])
        
    
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
}

class ProceedButtonCell: FieldBaseCell {
    
    let centerLabel = UILabel()
    let activityIndicator = UIActivityIndicatorView()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.configure()
        addCenterLabel()
        addActivityIndicator()
    }
    
    private func addCenterLabel()
    {
        centerLabel.textColor = UIColor.white
        centerLabel.textAlignment = .center
        centerLabel.text = "Proceed to Book"
        centerLabel.font =  UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.medium)
        
        centerLabel.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(centerLabel)
        
        
        NSLayoutConstraint.activate([
            
            centerLabel.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor, constant:  -5),
            centerLabel.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor),
            
//            centerLabel.heightAnchor.constraint(equalToConstant: 25.0)
        ])
        
        
    }
    
    private func addActivityIndicator()
    {
        activityIndicator.stopAnimating()
        activityIndicator.color = UIColor.white
        activityIndicator.style = UIActivityIndicatorView.Style.large

        activityIndicator.transform = CGAffineTransform.init(scaleX: 0.7, y: 0.7)
        
        self.contentView.addSubview(activityIndicator)
        
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
        activityIndicator.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor, constant:  -5),
        activityIndicator.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor),
        ])
        
    }
    public func submitStarted()
    {
        UIView.animate(withDuration: 0.3) {
            self.centerLabel.alpha = 0.1
            self.activityIndicator.startAnimating()
        }
        
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
