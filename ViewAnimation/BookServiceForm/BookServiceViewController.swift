//
//  BookServiceViewController.swift
//  ViewAnimation
//
//  Created by Riyaz Mohammed on 26/11/19.
//  Copyright © 2019 Riyaz Mohammed. All rights reserved.
//

import UIKit

class BookServiceViewController: UIViewController {
    
    
    var dataSource : BookServiceDataSource?
    var delegate : BookServiceDelegate?
    
    
    private let primaryCellHeight   = 200
    private let secondaryCellHeight  = 80
    private let submitButtonHeight  = 60
    
    private let threshHoldBottomMargin  = 300
    
    
    private let maxNumofRows = 5
    private let tableView = UITableView()
    private let selectionVC = SelectionViewController()
    private  let backButton = UIButton.init()
    private let titleLabel = UILabel()
    private let loader =  UIActivityIndicatorView()
    
    private  var heightConstraint : NSLayoutConstraint!
    private  var topFixConstraint : NSLayoutConstraint!
    private  var topFloatConstaint : NSLayoutConstraint!
    private var centerXContraint : NSLayoutConstraint!
    private var centerYConstraint : NSLayoutConstraint!
    
    
    private var currentFieldType = BookServiceField.FieldType.ServiceCentre
    
    private var numOfRows = 1
    override func viewDidLoad()
    {
        super.viewDidLoad()
        navigationController?.navigationBar.barTintColor = UIColor(red: 0.235, green: 0.231, blue: 0.373, alpha: 1)
        navigationController?.navigationBar.isOpaque = true
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        
        self.view.backgroundColor = UIColor(red: 239.0/255.0, green: 239.0/255.0, blue: 244.0/255.0, alpha: 1)
        
        setupTitleLabel()
        showPageLoader()
        setupBackButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    
    func formFetched()
    {
        loader.stopAnimating()
        tableView.alpha = 0
        selectionVC.view.alpha = 0
        
        addTableView()
        addSelectionView()
        self.view.bringSubviewToFront(tableView)
        
        let transform = CATransform3DTranslate(CATransform3DIdentity, 0, 300, 0)
        self.tableView.layer.transform = transform
        self.selectionVC.view.layer.transform = transform
        
        UIView.animate(withDuration: 0.3, animations: {
            self.tableView.alpha = 1
            
            let transform = CATransform3DTranslate(CATransform3DIdentity, 0, 0, 0)
            self.tableView.layer.transform = transform
        }) { (suc) in
            
            
        }
        
        UIView.animate(withDuration: 0.4, animations: {
            self.selectionVC.view.alpha = 1
            
            let transform = CATransform3DTranslate(CATransform3DIdentity, 0, 0, 0)
            self.selectionVC.view.layer.transform = transform
        }) { (suc) in
            
            
        }
        
        
    }
    
    public func formSubmitted()
    {
        func deleteAllRows()
        {
            var rowsToDelete = [IndexPath]()
            for i in 0..<numOfRows
            {
                rowsToDelete.append(IndexPath.init(row: i, section: 0))
            }
            numOfRows = 0
            tableView.deleteRows(at: rowsToDelete, with: UITableView.RowAnimation.middle)
        }
        
        func showSuccessView()
        {
            let successVC = SubmitSuccessViewController()
            self.addChild(successVC)
            successVC.view.alpha = 0
            self.view.addSubview(successVC.view)
            
            successVC.view.translatesAutoresizingMaskIntoConstraints = false
            
            NSLayoutConstraint.activate([
                successVC.view.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0),
                successVC.view.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0),
                successVC.view.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: -20),
                successVC.view.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            ])
            
            let transform = CATransform3DTranslate(CATransform3DIdentity, 0, -200, 0)
            successVC.view.layer.transform = transform
            
            UIView.animate(withDuration: 0.3, animations: {
                
                successVC.view.alpha = 1
                
                let transform = CATransform3DTranslate(CATransform3DIdentity, 0, 0, 0)
                successVC.view.layer.transform = transform
            }) { (suc) in
                
                
            }
            
        }

         deleteAllRows()
         showSuccessView()
         
         UIView.animate(withDuration: 0.3, animations: {
         self.tableView.alpha = 0
         
         self.heightConstraint.constant = 5
         self.tableView.layoutIfNeeded()
         
         }) { (success) in
        
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                if let mySceneDelegate = self.view.window?.windowScene?.delegate as? SceneDelegate
                {
                    mySceneDelegate.showHome()
                }
            })
            
        }

    }
    private func showPageLoader()
    {
        loader.startAnimating()
        self.view.addSubview(loader)
        loader.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            
            loader.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            loader.centerYAnchor.constraint(equalTo: self.view.centerYAnchor)
        ])
        
        
    }
    
    
    private func setupTitleLabel()
    {
        titleLabel.text = "Book Service"
        
        titleLabel.textColor = UIColor.black
        titleLabel.font = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.semibold)
        
        self.view.addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        
        NSLayoutConstraint.activate([
            titleLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            titleLabel.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor ,constant: 20),
            titleLabel.bottomAnchor.constraint(lessThanOrEqualTo: self.view.bottomAnchor),
            titleLabel.heightAnchor.constraint(equalToConstant:20),
            
        ])
        
        
    }
    private func setupBackButton()
    {
        
        backButton.setImage(#imageLiteral(resourceName: "back"), for: UIControl.State.normal)
        backButton.addTarget(self, action: #selector(goBack), for: UIControl.Event.touchUpInside)
        self.view.addSubview(backButton)
        backButton.translatesAutoresizingMaskIntoConstraints = false
        
        
        NSLayoutConstraint.activate([
            backButton.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor ,constant: 20),
            backButton.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 20),
            backButton.rightAnchor.constraint(lessThanOrEqualTo: self.view.rightAnchor),
            backButton.bottomAnchor.constraint(lessThanOrEqualTo: self.view.bottomAnchor),
            backButton.heightAnchor.constraint(equalToConstant:20),
            
        ])
        
        
    }
    @objc private func goBack()
   {
    self.navigationController?.popViewController(animated: true)
    }

    private func addTableView()
    {
        self.tableView.register(UINib(nibName: "VehicleCell", bundle: nil), forCellReuseIdentifier: "Vehicle")
        self.tableView.register(UINib(nibName: "ChoiceFieldCell", bundle: nil), forCellReuseIdentifier: "ChoiceFieldCell")
        tableView.backgroundColor = UIColor.clear
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        
        self.view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        heightConstraint = tableView.heightAnchor.constraint(equalToConstant: 200)
        topFixConstraint =     tableView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: CGFloat(secondaryCellHeight))
        topFloatConstaint =      tableView.topAnchor.constraint(greaterThanOrEqualTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0)
        centerXContraint = tableView.centerXAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.centerXAnchor)
        
        centerYConstraint     = tableView.centerYAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.centerYAnchor)
        
        
        
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 30),
            tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -30),
            topFixConstraint,
            tableView.bottomAnchor.constraint(lessThanOrEqualTo: self.view.bottomAnchor),
            heightConstraint
        ])
    }
    
    
    private func addSelectionView()
    {
        selectionVC.delegate = self
        selectionVC.view.clipsToBounds = true
        selectionVC.view.layer.cornerRadius = 30
        selectionVC.view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        self.addChild(selectionVC)
        self.view.addSubview(selectionVC.view)
        
        selectionVC.view.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            selectionVC.view.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0),
            selectionVC.view.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0),
            selectionVC.view.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 120 +  CGFloat(secondaryCellHeight)),
            selectionVC.view.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
        ])
        //        selectionVC.refreshPosition()
        
    }
    
    private func proceedToBook(indexPath : IndexPath)
    {
        
        
        guard let cell : ProceedButtonCell =  tableView.cellForRow(at: indexPath) as? ProceedButtonCell else
        {
            return
        }
        cell.submitStarted()
        
        dataSource?.submitForm()
        
        /*
         deleteAllRows()
         showSuccessView()
         
         UIView.animate(withDuration: 0.3, animations: {
         self.tableView.alpha = 0
         
         self.heightConstraint.constant = 5
         self.tableView.layoutIfNeeded()
         
         }) { (success) in
         }
         */
        
    }
    
    private func deleteRowsToStartSelectingAgain( indexPath : IndexPath , completion : @escaping (Bool) -> Void?)
    {
        
        
        let group = DispatchGroup()
        
        var toDelete = [IndexPath]()
        
        for i in (indexPath.row ) ..< self.numOfRows
        {
            group.enter()
            
            self.numOfRows = self.numOfRows - 1
            let   indexPathToDelete = IndexPath.init(row: i, section: 0)
            toDelete.append(indexPathToDelete)
            
            
            if let cell = tableView.cellForRow(at: indexPathToDelete)
            {
                UIView.animate(withDuration: 0.3, animations: {
                    cell.alpha = 0
                    cell.textLabel?.alpha = 0
                    cell.detailTextLabel?.alpha = 0
                    
                    let transform = CATransform3DTranslate(CATransform3DIdentity, 0, 60, 0)
                    
                    cell.layer.transform = transform
                }) { (suc) in
                    
                    group.leave()
                }
            }
        }
        
        group.notify(queue: .main) {
            
            self.tableView.deleteRows(at:toDelete, with: UITableView.RowAnimation.none)
            completion(true)
        }
        
    }
    private func revaluateFieldType(indexPath : IndexPath)
    {
        currentFieldType = BookServiceField.FieldType(rawValue: indexPath.row) ?? .none
    }
    
    private func makeTableViewHeightfill()
    {
        //               UIView.animate(withDuration: 0.2) {
        
        let height = self.primaryCellHeight + ( (self.maxNumofRows - 2) * self.secondaryCellHeight) + submitButtonHeight
        
        self.heightConstraint.constant = CGFloat(height)
        
        //                selectionVC.removeAllViews()
        //                self.heightConstraint.isActive = false
        
        self.topFixConstraint .isActive = false
        self.topFloatConstaint.isActive = true
        self.centerXContraint.isActive = true
        self.centerYConstraint.isActive = true
        self.tableView.layoutIfNeeded()
        self.tableView.isScrollEnabled = false
        //                     }
        
        
        
    }
    
    private func revaluateTableViewHeight()
    {
        
        //        UIView.animate(withDuration: 0.2) {
        
        var height = self.primaryCellHeight + ( (self.numOfRows - 1) * self.secondaryCellHeight)
        
        if height > Int(self.view.bounds.size.height) - self.threshHoldBottomMargin
        {
            height = Int(self.view.bounds.size.height) - self.threshHoldBottomMargin
        }
        
        
        self.heightConstraint.constant = CGFloat(height)
        self.topFixConstraint .isActive = true
        self.heightConstraint.isActive = true
        self.centerXContraint.isActive = false
        self.centerYConstraint.isActive = false
        self.topFloatConstaint.isActive = false
        //            self.tableView.layoutIfNeeded()
        //        }
        if numOfRows == maxNumofRows
        {
            makeTableViewHeightfill()
        }
    }
    private func choiceCellTitles(index : Int) -> (String,String)
    {
        guard let type : BookServiceField.FieldType = BookServiceField.FieldType.init(rawValue: index) else
        {
            return ("","")
        }
        
        switch type {
        case .Vehicle:
            return ("","")
        case .ServiceCentre,.ServiceType:
            if let choices = dataSource?.choices(fieldType: type)
            {
                if let selected = dataSource?.selectedChoiceIndex(fieldType: type)
                {
                    return (type.title() , choices[selected])
                }
//                if index < choices.count {
//
//                    let selected = choices[index]
//                    let values = selected.split(separator: "-")
//
//                    if values.count > 1 {
//
//                        return (String(values[0]),String(values[1]))
//                    }
//                }
            }
            return ("","")
            
        case .PickupTime:
            
            return (type.title() , dataSource?.selectedDate(fieldType: type)?.string(format: "dd-MMM-yyyy hh:mm:ss") ?? Date().string(format: "dd-MMM-yyyy hh:mm:ss"))
        default:
            return ("","")
            
        }
        
    }
    
    private  func performActionsAfterValueSelectionFromSectionView() {
        
        if numOfRows >= maxNumofRows
        {
            return
        }
        let indexpathsToInsert =  rowsToInsertRowsAfterSelection()
        self.tableView.insertRows(at: indexpathsToInsert, with: UITableView.RowAnimation.middle)
        
        animateRowsOnInsert(indexpathsToInsert: indexpathsToInsert)
        revaluateTableViewHeight()
        
        tableView.scrollToRow(at: IndexPath.init(row: numOfRows - 1, section: 0), at: UITableView.ScrollPosition.bottom, animated: true)
        
        if currentFieldType != .none
        {
            currentFieldType =   BookServiceField.FieldType(rawValue: currentFieldType.rawValue + 1) ?? .none
        }
        
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
        //
        //            self.selectionVC.refreshPosition()
        //        })
        //
        
    }
    
    private func rowsToInsertRowsAfterSelection() -> [IndexPath]
    {
        var indexpathsToInsert = [IndexPath]()
        
        if numOfRows == maxNumofRows - 2
        {
            numOfRows = numOfRows + 1
            indexpathsToInsert.append(IndexPath.init(row: self.numOfRows - 1 , section: 0))
            numOfRows = numOfRows + 1
            indexpathsToInsert.append(IndexPath.init(row: self.numOfRows - 1 , section: 0))
        }
        else
        {
            numOfRows = numOfRows + 1
            indexpathsToInsert.append(IndexPath.init(row: self.numOfRows - 1 , section: 0))
        }
        
        return indexpathsToInsert
        
    }
    private  func animateRowsOnInsert(indexpathsToInsert : [IndexPath])
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
            print("count \(indexpathsToInsert.count)")
            for indexpath in indexpathsToInsert
            {
                if let cell = self.tableView.cellForRow(at: IndexPath.init(row:indexpath.row , section: 0))
                {
                    
                    if let formCell = cell as? BookServiceFieldCell
                    {
                        
                        print(indexpath.row + 1)
                        formCell.textLabel?.alpha = 0
                        formCell.detailTextLabel?.alpha = 0
                        UIView.animate(withDuration: 0.2) {
                            formCell.textLabel?.alpha = 1
                            formCell.detailTextLabel?.alpha = 1
                        }
                    }
                    
                }
                
            }
        })
        
        
        
    }
    
}

extension BookServiceViewController : UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numOfRows
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0
        {
            return
        }
        
        cell.alpha = 0
        cell.textLabel?.alpha = 0
        cell.detailTextLabel?.alpha = 0
        let transform = CATransform3DTranslate(CATransform3DIdentity, 0, 60, 0)
        
        cell.layer.transform = transform
        
        UIView.animate(withDuration: 0.3) {
            cell.alpha = 1
            cell.textLabel?.alpha = 1
            cell.detailTextLabel?.alpha = 1
            
            cell.layer.transform = CATransform3DIdentity
            
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let identifier = "cell \(indexPath.row)"
        
        
        var cell : BookServiceFieldCell!
        
        if indexPath.row == 0
        {
            cell  = tableView.dequeueReusableCell(withIdentifier: "Vehicle", for: indexPath) as! VehicleCell
            cell.selectionStyle = .none
            (cell as! VehicleCell).setVehicleInfo(data: dataSource?.dataForVechicleCell())
            
            (cell as! VehicleCell).setVehicleInfo(data: dataSource?.dataForVechicleCell())
        }
        else if indexPath.row ==  (maxNumofRows - 1) // SubmitButton
        {
            if cell == nil
            {
                cell = ProceedButtonCell.init(style: UITableViewCell.CellStyle.default, reuseIdentifier: identifier)
            }
            
            cell.setIndex(index: indexPath.row)
        }
        else
        {
            
            if cell == nil
            {
                cell = ChoiceFieldCell.init(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: identifier)
            }
            cell.setIndex(index: indexPath.row)
            
            
            let titles = choiceCellTitles(index: indexPath.row)
            
            (cell as! ChoiceFieldCell).customTextLabel.text = titles.0
            (cell as! ChoiceFieldCell).customDetailedTextLabel.text = titles.1
            
            
        }
        cell.layer.zPosition = CGFloat(10 - (indexPath.row))
        cell.selectionStyle = .none
        return cell
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0
        {
            return CGFloat(primaryCellHeight)
            
        }
        else if indexPath.row == maxNumofRows - 1
        {
            return CGFloat(submitButtonHeight)
            
        }
        else
        {
            return CGFloat(secondaryCellHeight)
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if indexPath.row == 0
        {
            return
        }
        
        if indexPath.row == maxNumofRows - 1
        {
            proceedToBook(indexPath: indexPath)
            return
        }
        
        self.revaluateFieldType(indexPath: indexPath)
        
        var allfieldsPreviouslyFilled = false
        
        if numOfRows == maxNumofRows
        {
            allfieldsPreviouslyFilled = true
        }
        
        deleteRowsToStartSelectingAgain(indexPath: indexPath) { (sucess) -> Void? in
            
            self.revaluateTableViewHeight()
            
            return nil
        }
        if allfieldsPreviouslyFilled == true
        {
            self.selectionVC.refreshPositionOnFieldValueCorrectionAfterAllInputisFilled()
        }
        else
        {
            self.selectionVC.refreshPositionOnFieldValueCorrection()
        }
    }
    
}

extension BookServiceViewController : SelectionViewControllerDelegate
{
    func choices(fieldType: BookServiceField.FieldType) -> [String]? {
        return  dataSource?.choices(fieldType: fieldType)
    }
    
    func selectedChoice(fieldType: BookServiceField.FieldType, index: Int) {
        
        delegate?.choiceSelected(atIndex: index, fieldType: fieldType)
        performActionsAfterValueSelectionFromSectionView()
    }
    
    func selectedDate(fieldType :BookServiceField.FieldType , date : Date){
        delegate?.dateSelected(date: date, fieldType: fieldType)
        performActionsAfterValueSelectionFromSectionView()
        
    }
    
    func initialChoiceIndex(fieldType: BookServiceField.FieldType) -> Int? {
        return dataSource?.selectedChoiceIndex(fieldType: fieldType)
    }
    
    func initialDate(fieldType: BookServiceField.FieldType) -> Date? {
        return dataSource?.selectedDate(fieldType: fieldType)
    }
    
    
    func heightOFTopView() -> Int {
        
        var height = primaryCellHeight + ( (numOfRows - 1) * secondaryCellHeight)
        
        if height > Int(self.view.bounds.size.height) - threshHoldBottomMargin
        {
            height = Int(self.view.bounds.size.height) - threshHoldBottomMargin
        }
        
        return height - 120
    }
    
    func currentInputFieldType() -> BookServiceField.FieldType {
        return currentFieldType
    }
    
    
}
