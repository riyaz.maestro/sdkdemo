//
//  SubmitSuccessViewController.swift
//  ViewAnimation
//
//  Created by Riyaz Mohammed on 03/12/19.
//  Copyright © 2019 Riyaz Mohammed. All rights reserved.
//

import UIKit

class SubmitSuccessViewController: UIViewController {

    @IBOutlet weak var container: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        container.layer.cornerRadius = 20
        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
