//
//  DateExtension.swift
//  ViewAnimation
//
//  Created by Riyaz Mohammed on 28/11/19.
//  Copyright © 2019 Riyaz Mohammed. All rights reserved.
//

import Foundation

extension Date
{
    func string(format : String) ->String
    {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        let dateString = formatter.string(from:self) // string purpose I add here
        return dateString
    }
    
    static func date(string : String , format : String) -> Date?
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from:string)
        return date
    }

}


