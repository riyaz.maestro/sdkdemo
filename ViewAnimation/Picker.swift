//
//  Picker.swift
//  ViewAnimation
//
//  Created by Riyaz Mohammed on 26/11/19.
//  Copyright © 2019 Riyaz Mohammed. All rights reserved.
//

import UIKit


protocol PickerDelegate  {
    func doneClicked(view: Picker)
}

class Picker: UIView {
    
    private let delegate : PickerDelegate!
    init(delegate : PickerDelegate) {
        self.delegate = delegate
        super.init(frame: CGRect.zero)
        self.backgroundColor = UIColor.white
        self.clipsToBounds = true
        self.layer.cornerRadius = 30
        self.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        
        let button = UIButton.init(type: UIButton.ButtonType.roundedRect)
        button.setTitle("Done", for: UIControl.State.normal)
        
        button.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(button)
        
        NSLayoutConstraint.activate([
            
            button.topAnchor.constraint(equalTo: self.topAnchor, constant: 30),
            button.bottomAnchor.constraint(lessThanOrEqualTo: self.bottomAnchor),
            button.widthAnchor.constraint(equalToConstant: 60),
            button.heightAnchor.constraint(equalToConstant: 30),
            button.leadingAnchor.constraint(greaterThanOrEqualTo: self.leadingAnchor),
            button.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -50)
            
            ]
        )
        
        button.addTarget(self, action: #selector(doneClicked), for: UIControl.Event.touchUpInside)
        

        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    @objc func doneClicked()
    {
        self.delegate.doneClicked(view: self)
    }
    

    

}
